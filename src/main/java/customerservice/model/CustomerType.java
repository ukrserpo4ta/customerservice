package customerservice.model;

public enum CustomerType {

	INDIVIDUAL, COMPANY;
	
}
